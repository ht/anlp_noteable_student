""" Course-specific init wrapper for NLTK """
from nltk.corpus import util, reader
from nltk.tokenize import RegexpTokenizer, SpaceTokenizer

import nltk.data, os.path
nltk.data.path.append(__file__.replace('%scorpus.py'%os.path.sep,''))


demo=util.LazyCorpusLoader(
  'demo', reader.PlaintextCorpusReader, r'(?!\.).*\.txt', encoding='utf8',
  nltk_data_subdir='.')

wsj=util.LazyCorpusLoader(
  'wsj', reader.PlaintextCorpusReader, r'(?!\.).*\.txt', encoding='utf8',
  nltk_data_subdir='.',
  sent_tokenizer=RegexpTokenizer('\n', gaps=True),
  word_tokenizer=SpaceTokenizer()
  )

del util, reader
