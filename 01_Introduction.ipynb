{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<pre style=\"float: right\">version 2.0.1</pre>\n",
    "# Introduction\n",
    "## Getting started with Jupyter notebooks\n",
    "\n",
    "The remaining labs in this course will be done using Jupyter notebooks so we will here introduce some of the basics of the notebook system. If you are already comfortable using notebooks or just would rather get on with the lab free to [skip straight to the exercises below](#Exercises).\n",
    "\n",
    "*Note: Jupyter notebooks are also known as IPython notebooks, so if you're familiar with* those *you're also good to go.*\n",
    "\n",
    "### Jupyter basics: dashboard and processes\n",
    "\n",
    "In launching this notebook you will have already come across the first key component of the Jupyter system - the notebook *dashboard* interface.\n",
    "\n",
    "You got here via the Noteable link on the course's Learn page.  Noteable is a web-based interface to the University's Jupyter service.\n",
    "\n",
    "Your way in is the notebook *dashboard*. This will appear something like this\n",
    "\n",
    "<img src='res/jupyter-dashboard.png' />\n",
    "\n",
    "The dashboard above is showing the `Files` tab, a list of files in the directory the notebook server was launched from. We can navigate in to a sub-directory by clicking on a directory name and back up to the parent directory by clicking the `..` link.\n",
    "\n",
    "As well as allowing you to launch existing notebooks, we can also view or edit text files such as `.py` source files, directly in the browser by opening them from the dashboard. The in-built text-editor is less-featured than a full IDE but is useful for quick edits of source files and previewing data files.\n",
    "\n",
    "The `Running` tab of the dashboard gives a list of the currently running notebook instances. This can be useful to keep track of which notebooks are still running and to shutdown (or reopen) old notebook processes when the corresponding tab has been closed.\n",
    "\n",
    "### The notebook interface\n",
    "\n",
    "Once you open a notebook from the `notebooks` folder, the top of your notebook page which comes up in the browser should appear something like this:\n",
    "\n",
    "<img src='res/jupyter-notebook-interface.png' />\n",
    "\n",
    "The name of the current notebook is displayed at the top of the page and can be edited by clicking on the text of the name. Displayed alongside this is an indication of the last manual *checkpoint* of the notebook file. On-going changes are auto-saved at regular intervals; the check-point mechanism is mainly meant as a way to recover an earlier version of a notebook after making unwanted changes. Note the default system only currently supports storing a single previous checkpoint despite the `Revert to checkpoint` dropdown under the `File` menu perhaps suggesting otherwise.\n",
    "\n",
    "<!--- As well as having options to save and revert to checkpoints, the `File` menu also allows new notebooks to be created in same directory as the current notebook, a copy of the current notebook to be made and the ability to export the current notebook to various formats.\n",
    "\n",
    "The `Edit` menu contains standard clipboard functions as well as options for reorganising notebook *cells*. Cells are the basic units of notebooks, and can contain formatted text like the one you are reading at the moment or runnable code as we will see below. The `Edit` and `Insert` drop down menus offer various options for moving cells around the notebook, merging and splitting cells and inserting new ones, while the `Cell` menu allow running of code cells and changing cell types.\n",
    "\n",
    "The `Kernel` menu offers some useful commands for managing the Python process (kernel) running in the notebook. In particular it provides options for interrupting a busy kernel (useful for example if you realise you have set a slow code cell running with incorrect parameters) and to restart the current kernel. This will cause all variables currently defined in the workspace to be lost but may be necessary to get the kernel back to a consistent state after polluting the namespace with lots of global variables or when trying to run code from an updated module and `reload` is failing to work.\n",
    "--->\n",
    "\n",
    "To the far right of the menu toolbar is a status indicator. When a dark filled circle is shown this means the notebook is currently busy and any further code cell run commands will be queued to happen after the currently running cell has completed. An open status circle indicates the notebook is currently idle.\n",
    "\n",
    "The final row of the top notebook interface is the notebook toolbar which contains shortcut buttons to some common commands such as clipboard actions and cell management. If you are interested in learning more about the notebook user interface you may wish to run through the `User Interface Tour` under the `Help` menu drop down.\n",
    "\n",
    "### Markdown cells: easy text formatting\n",
    "\n",
    "This entire introduction has been written in what is termed a *Markdown* cell of a notebook. [Markdown](https://en.wikipedia.org/wiki/Markdown) is a lightweight markup language intended to be readable in plain-text. As you may wish to use Markdown cells to keep your own formatted notes in notebooks, you can look at [a *very* small sampling of the formatting syntax](res/md.html)  (escaped mark-up on top and corresponding rendered output below that); there are many much more extensive syntax guides - for example [this cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).\n",
    " \n",
    "### Code cells: in browser code execution\n",
    "\n",
    "Up to now we have not seen any runnable code. An example of a executable code cell is below. To run it first click on the cell so that it is highlighted, then either click the <i class=\"fa-step-forward fa\"></i> button on the notebook toolbar, go to `Cell > Run Cells` or use the keyboard shortcut `Ctrl+Enter`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "\n",
    "print('Hello world!')\n",
    "print('Alarming hello!', file=sys.stderr)\n",
    "print('Hello again!')\n",
    "'And again!'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This example shows the three main components of a code cell.\n",
    "\n",
    "The most obvious is the input area. This (unsuprisingly) is used to enter the code to be run which will be automatically syntax highlighted.\n",
    "\n",
    "To the immediate left of the input area is the execution counter. Before a code cell is first run this will display `In [ ]:`. After the cell is run this is updated to `In [n]:` where `n` is a number corresponding to the current execution counter which is incremented whenever any code cell in the notebook is run. This can therefore be used to keep track of the relative order in which cells were last run. There is no fundamental requirement to run cells in the order they are organised in the notebook, though things will usually be more readable if you keep things in roughly in order!\n",
    "\n",
    "Immediately below the input area is the output area. This shows any output produced by the code in the cell. This is dealt with a little bit confusingly in the current Jupyter version. At the top any output to [`stdout`](https://en.wikipedia.org/wiki/Standard_streams#Standard_output_.28stdout.29) is displayed. Immediately below that output to [`stderr`](https://en.wikipedia.org/wiki/Standard_streams#Standard_error_.28stderr.29) is displayed. All of the output to `stdout` is displayed together even if there has been output to `stderr` between as shown by the suprising ordering in the output here. \n",
    "\n",
    "The final part of the output area is the *display* area. By default this will just display the returned output of the last Python statement as would usually be the case in a (I)Python interpreter run in a terminal."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color: red; font-style: italic\">Stuff about separate iPython window [currently not working] hidden here, pbly not needed, double-click to see/edit</span>\n",
    "<!-- ### iPython console window\n",
    "We'll use code cells in notebooks for a lot of our python work, but sometimes it will be useful to have a place to run some python without cluttering up the notebook, or losing sight of the output when we modify the cell and run it again.  For this purpose we'll want to have a separate window that's just running iPython in our **class** environment.\n",
    "Here's how to do this\n",
    "\n",
    "<span style=\"color: red; float: left\">[W]</span>\n",
    "```\n",
    " Start > Programs > Anaconda3 > Anaconda Prompt\n",
    "    >activate class\n",
    "    >cd ...\\class   # this should happen automatically...\n",
    "    >ipython\n",
    "    In [1]: import nltk```\n",
    "<span style=\"color: red; float: left\">[M]</span>\n",
    "```\n",
    " Launchpad > Terminal\n",
    "    $ source activate class\n",
    "    $ cd .../class   # this should happen automatically...\n",
    "    $ ipython\n",
    "    In [1]: import nltk```\n",
    "where by `...\\class` and `.../class` is meant whatever you need to do to get to your **class** directory if it hasn't happened automatically.-->"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%qtconsole"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises\n",
    "\n",
    "Today's exercises are meant to allow you to get some initial familiarisation with processing natural language data using NLTK. \n",
    "\n",
    "If you are new to Python and are struggling to understand the exercises, you may find if helpful to work through a locally created [Python3 version of a Stanford University introduction](python_intro.ipynb).  If you would like to check/refresh your maths background, please have a look at [Sharon Goldwater's three excellent short tutorials](http://homepages.inf.ed.ac.uk/sgwater/math_tutorials.html), which cover vectors, sets and probability. \n",
    "\n",
    "### Exercise 1 \n",
    "\n",
    "[NLTK](http://www.nltk.org/book/) provides a wide range of basic tooling for working with natural language data.  It's not optimised for efficiency, but for clarity, as it's intended primarily as a teaching tool.  We will use it extensively during this masterclass.  We have a few local customisations, so we always start our examples by importing them, which also provides us with nltk itself, as well as a number of builtin corpora.  The first of these is just a plaintext version of the four successive versions of the School of Informatics forward plan, which is available via a corpus object which we've preloaded into `demo`.  Let's have a look:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from data.corpus import demo # Our first dataset\n",
    "\n",
    "demo.fileids()    # all the data files which make up the corpus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's a python list, with the names of the files that are available in the `charter` corpus.  To find out what else NLTK corpora make available to you, you can use the builtin `help` facility.\n",
    "\n",
    "### Getting help in a notebook (and in python in general)\n",
    "\n",
    "Before moving on, let's make sure you know how to get help. There are several ways to do this:\n",
    "\n",
    "For help on almost any bit of python, including module names (e.g. `sys`), functions (e.g. `print` or `os.getcwd`), classes (e.g. `int`), methods (e.g. `str.lower`) or variables, sometimes too *much* help, use `help(...)` from the iPython prompt, e.g. `help(str.lower)`.  Whenever a long output shows `-- More  --` you can cut it short with a `q` (for 'quit').\n",
    "\n",
    "To get a list of all the methods available for an object, you can use tab completion in iPython. For example, type the following two lines into iPython, but don't hit <enter> after the second line:\n",
    "```python\n",
    "ss = \"a string\"\n",
    "ss.```\n",
    "Now type `<tab>`. You should see a list of all of the methods you can call on `ss`, which is to say, all the methods for strings.  If you type enough more letters to completely disambiguate your choice, and another `<tab>`, the rest will be filled in.  Try `ss.<tab>cap<tab>()<enter>`\n",
    "\n",
    "Even better, at any point after the first `<tab>`, you can `<tab>` again to cycle through the available alternatives, which will be highlighted in turn, and then by hitting `<enter>` you can pick that one.\n",
    "\n",
    "Python's [own documentation](https://docs.python.org/3/library/index.html) is very good, and the name 'Library' notwithstanding covers all the builtin functions and classes as well as all the library modules.\n",
    "\n",
    "`<tab>` also works in any code cell in a notebook:  if you position the cursor almost anywhere and `<tab>`, available completions will be shown.  See below at [Useful keyboard shortcuts](#Useful-keyboard-shortcuts) for the use of `<alt>+<tab>` in notebooks for even more information.\n",
    "#### Back to the exercise: inside a corpus\n",
    "\n",
    "Try using tab completion to find out what you can do with an NLTK corpus, by entering `charter.<tab>`.  `charter.words` looks interesting---try `help(charter.words)`.\n",
    "OK, lets try that.  Run the code cell below first, to remind ourselves what files we have in the charter corpus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "demo.fileids()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "demo.words('school_of_informatics_strategy_and_plan_2017-20.txt')  # NLTK splits the file into white-space separated tokens"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's a list of the words in the first report.  But not all of it---iPython has helpfully truncated what would otherwise have been a very long list.  How long?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(demo.words('school_of_informatics_strategy_and_plan_2017-20.txt')) # How long is the list of tokens?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the whole list of ids to get all the lengths:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "[len(demo.words(f)) for f in demo.fileids()] # Collect the lengths of all the files in the corpus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can of course actually *look* at the corpus by switching back to the Jupyter dashboard tab in your browser and navigating up to the top folder then down through `data` and `demo` to `school_of_informatics_strategy_and_plan_2017-20.txt` -- give that a try to see a more easy-to-read version of what we're looking at.\n",
    "\n",
    "#### Help for the slow/lazy typists among us\n",
    "\n",
    "It's irritating to have to type or copy-paste that long filename again to get the length of the wordlist when you've already just got the wordlist on the previous line.  If you knew ahead of time you could save the value before you look at it, using\n",
    "\n",
    "```\n",
    "wl=demo.words('school_of_informatics_strategy_and_plan_2017-20.txt')  # Save the list of words in a variable\n",
    "wl                                                # What's in that variable?\n",
    "len(wl)                                           # What's its length?```\n",
    "\n",
    "Try that (one line at a time) in your iPython window.\n",
    "\n",
    "Alternatively, when you didn't think ahead, iPython saves all the output it shows you in special variables, so you can re-use it.  In particular, `_` (a single underscore) always has the value of the most recent output.  So you could have, as it were, eaten your cake and then remembered you still wanted to have it.  Try this instead:\n",
    "\n",
    "```\n",
    "wl=demo.words('school_of_informatics_strategy_and_plan_2017-20.txt')\n",
    "len(_)                                           # What's the length of the most recent output?```\n",
    "\n",
    "To reach further back, you can use `_nnn` to get the output from the line labelled `Out[nnn]: `."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Counting words, types and tokens"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 1\n",
    "\n",
    "We've found the word count for each of the files.  Speaking more carefully, what we've actually counted is word **tokens**, that is, individual occurrences.  Another thing we might like to know is the number of word **types**, that is, not the size of the text, but the size of its vocabulary:  how many *distinct* words are there?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we'll get that list of files one last time, and save it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ids=demo.fileids()\n",
    "ids"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class=\"tip\"><span>Throughout these labs we'll give you hints, extra things for the more experiencedto try (labelled 'going further') and other notes you can see by hovering over them whenever you see something like this on the right-hand side of the page.</span>Hover over me, please</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the next cell to see the token and type counts for each file---can you see where the difference lies?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for id in ids:                                   # Loop over all files in the corpus\n",
    "    words=demo.words(id)                      # get the list just once, for use twice\n",
    "    print(id,len(words),len(set(words)))         # print filenames and a pair of lengths of ... what?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class=\"tip\"><span>We've used python's builtin support for _sets_, which don't allow duplicates, so no matter how many times a word shows up in the `words` list,\n",
    "it's still only in the _set_ made from that list once.</span>Hover for the answer</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Insofar as we're interested in vocabulary, there's something at least unhelpful about the above numbers.  Lets look at the first charter's set:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c0_types=set(demo.words(ids[0]))      # A set of all the word types in the first file in the corpus\n",
    "c0_types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python lets us test the membership of sets, and we can use this to illuminate the issue:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "[(w in c0_types) for w in ['school', 'The', 'School', 'the']]  # For each of the example words\n",
    "                                                                 # test if it's in our set, and\n",
    "                                                                 # collect the results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The words 'charter' and 'the' are turning up in the set of types twice, in a way, because they occur capitalised as well as in lower-case.  For a more accurate count, we should fix this.  Before looking at the next cell, think for a moment as to how we might change the counting loop to avoid this double counting?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Important note**: If at any point during these labs, you don't understand how some code cell actually achieves the results it shows, and your partner doesn't either, *ask us*.  That's what we're here for!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for id in ids:                                            # Loop over all files in the corpus\n",
    "    words=[w.lower() for w in demo.words(id)]          # Collect the lower-case version of each word\n",
    "    print(id,len(words),len(set(words)))                  # Same as before, with the lower-cased list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class='tip'><span>Modify this loop to include a column showing the year.</span>Going further (python)</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So the type numbers are a bit less---for the right reason?  We can check:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c0_lc_types=[w.lower() for w in c0_types]           # Convert the list we saved earlier to lower-case\n",
    "c0_lc_types"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "[(w in c0_lc_types) for w in ['school', 'The', 'School', 'the']] # And check our test words again"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that 'School' and 'The' are gone from the list, which is right."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class='tip'><span>But we've also changed the surname 'Homer' to 'homer', which is wrong.  Using the `corpus.sentences` method, do a better job by lower-casing only the first word in each sentence.</span>Going further (python)</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 2\n",
    "\n",
    "Are these reports getting simpler or harder as the years go by?  One very simple measure of text difficulty is the average length of the words in its vocabulary.\n",
    "\n",
    "Run the following cell to find the average word type length in each of the reports:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for id in ids:\n",
    "    ts=set(w.lower() for w in demo.words(id))  # A set of the (lower-case) words the current year\n",
    "    n=len(ts)                                     # The number of word types\n",
    "    total_type_length=sum(len(w) for w in ts)     # The sum of the lengths of all the word types\n",
    "    print(\"%d %4.2f\"%(n,total_type_length/n))     # Print the count and the average\n",
    "                                                  # The funny percent stuff allows us to specify\n",
    "                                                  #  formatting for the two numbers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class='tip'><span>See [this simple introduction](res/percent.html) or the [Python documentation itself](https://docs.python.org/3/library/stdtypes.html#old-string-formatting) for more detail.</span>More on controling format</span> "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class='tip wide'><span>How do these reports compare to other types of language use?  Try computing word type length averages for one of the other NLTK datasets we've supplied:<br/>\n",
    "<code>from nltk.corpus import gutenberg, brown</code></span>Going further (different data)</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Is that increase in the final year meaningful?  We won't get to the question of (statistical) *significance* until later in the course."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Frequency distributions\n",
    "\n",
    "A frequency distribution records the number of times different events occur or objects are observed. We can use a frequency distribution to\n",
    "record the number of times each word appears in a document:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run this cell to obtain the frequency distribution for the selected file, using the `FreqDist` class from NLTK:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nltk\n",
    "fd = nltk.FreqDist(w.lower() for w in demo.words(ids[0]))  # Build a FreqDist from the first report\n",
    "                                                              #  in the corpus\n",
    "for word, freq in list(fd.items())[:5]:                       # Just a sample of the word/count pairs\n",
    "    print(word, freq)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class='tip'><span>If you're unsure about the `[0]` or the `[:5]` in the above, take a quick look at [this introduction to lists and slicing](http://localhost:8888/notebooks/notebooks/python_intro.ipynb#Lists).</span>Unsure about what the `[…]` is doing?</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `fd.most_common` method will give you the words and their counts in top-down order of frequency:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for word, freq in fd.most_common(10):  # fd is the frequency distribution we created in the previous cell\n",
    "    print(word, freq)                  # The most-common method returns a list of pairs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run this cell to plot the frequencies of the 50 most frequently used words in the file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as pyp   # Short name for the python default plotter\n",
    "fig = pyp.figure(figsize=(10,6))  # This makes the next plot be a good shape\n",
    "fd.plot(50)               # Plot the 50 most common word counts\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class='tip'><span>The plot is probably hidden behind other windows on your desktop.  Check the taskbar at the bottom of your screen for a round white icon with a drawing inside, or try shrinking other big windows to get them out of the way.</span>What if nothing happens?</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As discussed in lecture, what we see in this graph is an example of **Zipf's law**:  the **distribution** of counts that we so often see with natural language data: a very steep fall followed by a long flat tail.\n",
    "\n",
    "The next cell uses all four charter files and shows more words, to give a smoother curve:  you can't easily read the words, but the shape is very clear:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = pyp.figure(figsize=(18,10))                                     # Need more room for this one...\n",
    "fd = nltk.FreqDist(w.lower() for i in ids for w in demo.words(i))  # Build a FreqDist from all the \n",
    "                                                                      #  reports in the corpus\n",
    "print(len(fd))                                                        # The total number of word types\n",
    "                                                                      #   in the whole corpus\n",
    "fd.plot(200)                                                          # Plot the top 200"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class='tip'><span>Use the zig-zag-upward arrow menu entry in the plot window to switch both x- and y-axes to a log-scale and look at the results:  nearly straight, diagnostic of a power-law relationship</span>Digging deeper(maths)</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What about the words themselves over time?  That is, has the vocabulary in use shifted at all?  The code below gives the position and count of the top 30 word types used in 2013 vs. those in 2016.  We've also added a test in the list comprehension to exclude things that aren't really words:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fd_2013 = nltk.FreqDist(w.lower() for w in demo.words(ids[0]) if w.isalpha()) # An FD for 2013 file\n",
    "fd_2016 = nltk.FreqDist(w.lower() for w in demo.words(ids[3]) if w.isalpha()) # And one for 2016\n",
    "print(\"Report vocabulary size: %d %d\"%(len(fd_2013),len(fd_2016)))  # Compare the vocab size\n",
    "print(\"rank        2013           2016   \")                         # Column headers\n",
    "top_2013=fd_2013.most_common(30)                                    # Top 30 types and counts for 2013\n",
    "top_2016=fd_2016.most_common(30)                                    # And for 2016\n",
    "for i in range(30):\n",
    "    print(\"%2d %10s %3d %10s %3d\"%               # Five things to print, so 5 formats\n",
    "                (i+1,                            # Python counts from 0, but we prefer top rank as 1\n",
    "                 top_2013[i][0],top_2013[i][1],  # Each entry in a top list is a pair of word and count\n",
    "                 top_2016[i][0],top_2016[i][1])) #  so we subscript with 0 and 1 to extract them"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Interesting, but a bit frustrating.  It doesn't tell us much to see that 'and' and 'to' and 'the' are very common.  Indeed it's hard to see the words that *matter*, the meaningful ones, because of all the little words that English grammar makes so much use of.  So for our final experiment, we'll make use of a list of those 'little words' provided by NLTK.  For historical reasons they're known as **stopwords**.  Before running the code below, give just a moments thought to what you think it will show."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from nltk.corpus import stopwords\n",
    "skip_these=stopwords.words('english') \n",
    "fd_2013 = nltk.FreqDist(w.lower() for w in demo.words(ids[0]) if (w.isalpha() and\n",
    "                                                                     w not in skip_these)) # <------\n",
    "fd_2016 = nltk.FreqDist(w.lower() for w in demo.words(ids[3]) if (w.isalpha() and\n",
    "                                                                     w not in skip_these)) # <------\n",
    "print(\"Report vocabulary size: %d %d\"%(len(fd_2013),len(fd_2016)))\n",
    "print(\"rank          2013             2016   \")\n",
    "top_2013=fd_2013.most_common(30)\n",
    "top_2016=fd_2016.most_common(30)\n",
    "for i in range(30):\n",
    "    print(\"%2d %12s %3d %12s %3d\"%(i+1,\n",
    "                                   top_2013[i][0],top_2013[i][1],\n",
    "                                   top_2016[i][0],top_2016[i][1]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span class='tip'><span>Only the two lines with the arrow comments are different from the previous version.  They allow only words *not* in the stopword list to be added in to the distribution.</span>Answer</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Not really enough data to say much conclusively, but it's tempting to wonder about why 'we' and 'us' have lost ground at the expense of 'hmrc', or how 'improvements' seem to have given way to 'commitments', or what might lie behind the reversal in position of 'customer(s)' and 'charter'.\n",
    "<span class=\"tip\"><span>Expand the table with columns for 2014 and 2015.  If you're familiar enough with Python, do this by defining a function which takes an id index as argument and returns a FreqDist, and use this to build the input to the print statement, otherwise just copy/paste/modify the lines with year numbers in them.</span>Going further</span>\n",
    "\n",
    "On a final lighter note, you've now seen just about all that's needed to feed into the hard layout work that lies behind those popular word jigsaws (actual figure courtesy of [python implemention by Andreas Mueller](https://github.com/amueller/word_cloud)): <img textgloss=\"word cloud for 2013 charter\" src=\"res/cloud.png\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Advanced notebook features\n",
    "\n",
    "### Useful keyboard shortcuts\n",
    "\n",
    "There are a wealth of keyboard shortcuts available in the notebook interface. For an exhaustive list see the `Keyboard Shortcuts` option under the `Help` menu. We will cover a few of those we find most useful below.\n",
    "\n",
    "Shortcuts come in two flavours: those applicable in *command mode*, active when no cell is currently being edited and indicated by a blue highlight around the current cell; those applicable in *edit mode* when the content of a cell is being edited, indicated by a green current cell highlight.\n",
    "\n",
    "In edit mode of a code cell, two of the more generically useful keyboard shortcuts are offered by the `<tab>` key.\n",
    "\n",
    "  * Pressing `<tab>` a single time while editing code will bring up suggested completions of what you have typed so far. This is done in a scope aware manner so for example typing `a+<tab>` in a code cell will come up with a list of objects beginning with `a` in the current global namespace, while typing `np.a+<tab>` (assuming `import numpy as np` has been run already) will bring up a list of objects in the root NumPy namespace beginning with `a`.\n",
    "  * Pressing `<shift>+<tab>` once immediately after opening parenthesis of a function or method will cause a tool-tip to appear with the function signature (including argument names and defaults) and its docstring. Pressing `<shift>+Tab` twice in succession will cause an expanded version of the same tooltip to appear, useful for longer docstrings. Pressing `<shift>+<tab>` four times in succession will cause the information to be instead displayed in a pager docked to bottom of the notebook interface which stays attached even when making further edits to the code cell and so can be useful for keeping documentation visible when editing e.g. to help remember the name of arguments to a function and their purposes.\n",
    "\n",
    "A series of useful shortcuts available in both command and edit mode are `<mod>+Enter` where `<mod>` is one of `<ctrl>` (run selected cell), `<shift>` (run selected cell and select next) or `<alt>` (run selected cell and insert a new cell after).\n",
    "\n",
    "A useful command mode shortcut to know about is the ability to toggle line numbers on and off for a code cell by pressing `L` which can be useful when trying to diagnose stack traces printed when an exception is raised or when referring someone else to a section of code.\n",
    "  \n",
    "### Magics\n",
    "\n",
    "There are a range of *magic* commands in IPython notebooks, than provide helpful tools outside of the usual Python syntax. A full list of the inbuilt magic commands is given [here](http://ipython.readthedocs.io/en/stable/interactive/magics.html), however three that are particularly useful for this course:\n",
    "\n",
    "  * [`%%timeit`](http://ipython.readthedocs.io/en/stable/interactive/magics.html?highlight=matplotlib#magic-timeit) Put at the beginning of a cell to time its execution and print the resulting timing statistics.\n",
    "  * [`%precision`](http://ipython.readthedocs.io/en/stable/interactive/magics.html?highlight=matplotlib#magic-precision) Set the precision for pretty printing of floating point values and NumPy arrays.\n",
    "  * [`%debug`](http://ipython.readthedocs.io/en/stable/interactive/magics.html?highlight=matplotlib#magic-debug) Activates the interactive debugger in a cell. Run after an exception has been occured to help diagnose the issue."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use the cell below to experiment further with the data and distributions we've used above, or load things into your ipython window and experiment there."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%qtconsole"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
