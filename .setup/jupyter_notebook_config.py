'''HST attempt at notebook init file'''

import sys,os,datetime,traceback
if os.getenv("NAAS_COURSE_ID")!='INFR111252020-1SV1SEM1':
    # Temporary while debugging
    with open("/home/jovyan/oops.txt","a") as f:
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),file=f)
        print(os.getenv("NAAS_COURSE_ID"),file=f)
    exit()
    
try:
    ct=type(c)
    # Should give something or we're in trouble
except:
    ct=None

def tf(model,**kwargs):
    if model['type'] != 'notebook':
        # only run on notebooks
        return
    with open("l_test.txt","a") as f:
       print('args',type(model),list(kwargs.keys()),file=f)
       print(model['content'],list(model.keys()),file=f)

with open("/home/jovyan/.config_log.txt","a") as f:
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),ct,'xyzzy',file=f)
    if ct is None:
        print('No config???',file=f)
    else:
        print('checking pre_save',file=f)
        # test pre_save hook
        try:
            if not (hasattr(c,'FileContentsManager') and
                    hasattr(c.FileContentsManager,'pre_save_hook') and
                    hasattr(c.FileContentsManager.pre_save_hook,'__code__') and
                    c.FileContentsManager.pre_save_hook.__code__.co_code==tf.__code__.co_code):
                # This always fails, not sure why
                print('Setting pre-save hook',file=f)
                c.FileContentsManager.pre_save_hook = tf
            else:
                print('Presave OK',file=f)
        except Exception as e:
            import traceback
            traceback.print_exc(file=f)

    print('checking for notebook_index...',file=f)
    try:
        import notebook_index
        print('got it',file=f)
    except:
        print('notebook_index missing, reinstalling',file=f)
        os.system('/home/jovyan/anlp_noteable_student/.setup/.index_install.sh')


