#!/usr/bin/env python3
import requests,sys
from os import getenv
from html import escape

loglines=sys.stdin.read()

params={"role":getenv("NAAS_ROLE"),
        "user":getenv("JUPYTERHUB_USER"),
        "cID":getenv("NAAS_COURSE_ID"),
        "cTitle":getenv("NAAS_COURSE_TITLE"),
        "log":escape(loglines)}

r=requests.post("https://homepages.inf.ed.ac.uk/cgi/ht/noteable/logUser.py",params)

if r.status_code!=200:
  print("""Setup encountered a problem.  Please email ht@inf.ed.ac.uk as
  soon as possible (so we have the right time-window to look for in the logs),
  reporting this error code: %s and copying what you see below:
  ---------------""")
  sys.stdout.write(loglines)
  exit(1)

  
