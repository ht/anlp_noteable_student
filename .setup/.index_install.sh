#!/bin/bash
# Support README.md
echo -n starting download...
pip install git+https://github.com/NII-cloud-operation/Jupyter-LC_index >/dev/null
echo -n installing...
jupyter nbextension install --sys-prefix --py notebook_index >/dev/null
jupyter nbextension enable --sys-prefix --py notebook_index >/dev/null
echo ok
