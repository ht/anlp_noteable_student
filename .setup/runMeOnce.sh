#!/bin/bash
if [ -d $HOME/ANLP ]
then
    echo "ANLP already exists, you did't need to run this, exiting"
    exit 1
fi
echo "Welcome to ANLP on Noteable, ${NAAS_ROLE}.
Some initial setup is now required.
You will be asked to login to git.ecdf.ed.ac.uk: use your
University UUN and password, as you would for e.g. Learn"
log=/tmp/anlp_setup
echo $(date) start setup >$log 2>&1
{ cd
cp -a anlp_noteable_student/.setup/jupyter_notebook_config.py .
anlp_noteable_student/.setup/check_custom.sh
cat > anlp_noteable_student/.gitignore <<EOF
.gitignore
.setup
EOF
git config --global credential.helper 'cache --timeout 3600'
if [ 1 -eq 1 ]
then
mkdir ANLP
cp -a anlp_noteable_student/* ANLP # doesn't copy .setup
else
# Clone an empty repo for the student
#git clone https://git.ecdf.ed.ac.uk/${NAAS_ROLE}/ANLP"
# Test as we don't have real UUN
#git clone https://git.ecdf.ed.ac.uk/ht/T040004.git ANLP
cp -a anlp_noteable_student/* ANLP # doesn't copy .setup
cd ANLP
git config user.email "${NAAS_ROLE}@sms.ed.ac.uk"
git config user.name "${NAAS_ROLE}"
git add *
git commit -m'initial setup'
git push
cd ..
fi
} >>$log 2>&1
anlp_noteable_student/.setup/.index_install 2>>$log
echo $(date) end setup >>$log 2>&1
anlp_noteable_student/.setup/.postSetup.py < $log && \
echo "Setup complete.
Return to your Jupyter dashboard and go back up to the root folder.
Refresh your browser (Control-R) and select the ANLP folder,
where you should find everything you need."
